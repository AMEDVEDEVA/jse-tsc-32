package ru.tsc.golovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractUserCommand;
import ru.tsc.golovina.tm.enumerated.Role;

public final class UserClearCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-clear";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear all users";
    }

    @Override
    public void execute() {
        serviceLocator.getUserService().clear();
    }

}
