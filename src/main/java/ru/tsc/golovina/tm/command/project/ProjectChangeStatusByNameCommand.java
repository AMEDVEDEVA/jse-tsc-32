package ru.tsc.golovina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractProjectCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "project-change-status-by-name";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by name";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        serviceLocator.getProjectService().changeStatusByName(userId, name, status);
    }

}
