package ru.tsc.golovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;

public interface IAuthRepository {

    @NotNull
    String getCurrentUserId();

    void setCurrentUserId(@NotNull String currentUserId);

}
