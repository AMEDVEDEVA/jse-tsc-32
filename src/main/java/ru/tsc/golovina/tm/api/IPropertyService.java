package ru.tsc.golovina.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

}
